//Nombre: Yennifer Carolina Rodriguez Sanchez
//Tema: Examen Corte 1 
//Fecha: 09 de junio de 2023
package examenc1;

public class Docente {
    private int numDocente;
    private String nombre;
    private String domicilio;
    private int nivel;
    private float pagoBase;
    private int horasImpartidas;
    
    //Contructor
        public Docente(int numeroDocente, String nombre, String domicilio, int nivel, double pagoBaseHora, int horasImpartidas) {
        this.numDocente = numeroDocente;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.nivel = nivel;
        this.pagoBase = pagoBase;
        this.horasImpartidas = horasImpartidas;
    }

    public int getNumDocente() {
        return numDocente;
    }

    public void setNumDocente(int numDocente) {
        this.numDocente = numDocente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public float getPagoBase() {
        return pagoBase;
    }

    public void setPagoBase(float pagoBase) {
        this.pagoBase = pagoBase;
    }

    public int getHorasImpartidas() {
        return horasImpartidas;
    }

    public void setHorasImpartidas(int horasImpartidas) {
        this.horasImpartidas = horasImpartidas;
    }
    
    //Metodos 
    public float calcularPago(){
        double pagoTotal = pagoBase + horasImpartidas;
        
        switch(nivel){
            case 1:
                pagoTotal += pagoTotal * 0.3;
                break;
            case 2:
                pagoTotal += pagoTotal * 0.5;
                break;
            case 3: 
                pagoTotal += pagoTotal;
                break;
        }
        return (float) pagoTotal;
    }
        /**
     *
     * @return
     */
    public float calcularImpuesto() {
        double pagoTotal = calcularPago();
        return (float) (pagoTotal * 0.16);
    }

    public double calcularBono(int cantidadHijos) {
        if (cantidadHijos >= 1 && cantidadHijos <= 2) {
            return calcularPago() * 0.05;
        } else if (cantidadHijos >= 3 && cantidadHijos <= 5) {
            return calcularPago() * 0.10;
        } else if (cantidadHijos > 5) {
            return calcularPago() * 0.20;
        } else {
            return 0;
        }
    }
}
